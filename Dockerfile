FROM nginx:1.12-alpine

ARG error_file=/var/log/nginx/error.log
ARG log_file=/var/log/nginx/error.log

RUN apk add --no-cache shadow \
&& groupadd -f -g 33 -o -r www-data || : \
&& useradd -d /var/www -G 33 -M -N -o -r -s /usr/bin/nologin -u 33 www-data \
&& apk del shadow

RUN mkdir -p $(dirname $error_file) \
&& rm -f $error_file && mkfifo -m006 $error_file \
&& rm -f $log_file && mkfifo -m006 $log_file

COPY usr/local/bin/piped-std /usr/local/bin/piped-std
ENTRYPOINT ["/usr/local/bin/piped-std"]
CMD ["nginx", "-g", "daemon off;"]
